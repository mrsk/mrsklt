
Ubuntu:
- `apt-get install gcc`
- `apt-get install gcc-multilib`
- `apt-get install pkg-config`
- `apt-get install libssl-dev`



Any:
- `cargo install --force cargo-make`
- `rustup target add wasm32-unknown-unknown`
- `cargo make start`


