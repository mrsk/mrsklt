
use seed;
use seed::prelude::*;
use crate::Msg;


pub fn view(content_html: &str) -> Node<Msg> {
    return seed::div![
        seed::style!{
            "color" => "rgb(85, 85, 85)",
            "font-family" => "Georgia",
            "font-size" => "16px",
            "line-height" => "24px"
        },
        seed::raw!(content_html)
    ]
}
