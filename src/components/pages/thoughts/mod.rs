
use seed;
use seed::prelude::*;
use crate::Msg;
use crate::components;
use crate::PageEnum;
use crate::ThoughtsPageEnum;

mod existance;
mod morality;

static CONTENT: &'static str = include_str!("../../html/thoughts.html");

pub fn view(thoughts_page: ThoughtsPageEnum) -> Node<Msg> {
    return match thoughts_page {
        ThoughtsPageEnum::Main => view_main(),
        ThoughtsPageEnum::Existance => existance::view(),
        ThoughtsPageEnum::Morality => morality::view(),
    }
}

pub fn view_main() -> Node<Msg> {
    seed::div![
        components::page_title::view("What I think about . . ."),
        components::page_content::view(CONTENT),
        view_thought_links()
    ]
}

fn view_thought_links() -> Vec<Node<Msg>> {
    return vec![
        existance::view_page_link(),
        morality::view_page_link(),
    ];
}

pub fn view_page_link() -> Node<Msg> {
    return components::page_link::view("What I think about . . .", &PageEnum::Thoughts(ThoughtsPageEnum::Main).href());
}
