use crate::components;
use crate::Msg;
use crate::PageEnum;
use crate::ThoughtsPageEnum;
use seed;
use seed::prelude::*;

static CONTENT: &'static str = include_str!("../../html/thoughts_morality.html");

pub fn view() -> Node<Msg> {
    seed::div![
        view_back_link(),
        components::page_title::view("My moral views."),
        components::page_content::view(CONTENT),
    ]
}

fn view_back_link() -> Node<Msg> {
    return components::page_link::view(
        "↩ back",
        &PageEnum::Thoughts(ThoughtsPageEnum::Main).href(),
    );
}

pub fn view_page_link() -> Node<Msg> {
    return components::page_link::view(
        "My moral views.",
        &PageEnum::Thoughts(ThoughtsPageEnum::Morality).href(),
    );
}
