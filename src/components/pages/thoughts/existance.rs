
use seed;
use seed::prelude::*;
use crate::Msg;
use crate::components;
use crate::PageEnum;
use crate::ThoughtsPageEnum;


static CONTENT: &'static str = include_str!("../../html/thoughts_existance.html");

pub fn view() -> Node<Msg> {
    seed::div![
        view_back_link(),
        components::page_title::view("Why does anything exist at all?"),
        components::page_content::view(CONTENT),
    ]
}



fn view_back_link() -> Node<Msg> {
    return components::page_link::view("↩ back", &PageEnum::Thoughts(ThoughtsPageEnum::Main).href())
}


pub fn view_page_link() -> Node<Msg> {
    return components::page_link::view("Why does anything exist at all?", &PageEnum::Thoughts(ThoughtsPageEnum::Existance).href());
}


