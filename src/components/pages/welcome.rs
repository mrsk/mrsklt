
use seed;
use seed::prelude::*;
use crate::Msg;
use crate::components;
use crate::PageEnum;

static CONTENT: &'static str = include_str!("../html/welcome.html");

pub fn view() -> Node<Msg> {
    seed::div![
        components::page_title::view("Welcome"),
        components::page_content::view(CONTENT)
    ]
}

pub fn view_page_link() -> Node<Msg> {
    return components::page_link::view("Welcome!", &PageEnum::Welcome.href());
}


