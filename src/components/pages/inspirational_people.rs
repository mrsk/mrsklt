
use seed;
use seed::prelude::*;
use crate::Msg;
use crate::components;
use crate::PageEnum;

static CONTENT: &'static str = include_str!("../html/inspirational_people.html");

pub fn view() -> Node<Msg> {
    seed::div![
        components::page_title::view("Inspirational people"),
        components::page_content::view(CONTENT)
    ]
}

pub fn view_page_link() -> Node<Msg> {
    return components::page_link::view("Inspirational people", &PageEnum::InspirationalPeople.href());
}


