
use seed;
use seed::prelude::*;
use crate::Msg;


pub fn view(title: &str) -> Node<Msg> {
    return seed::div![
        seed::style!{
            "color" => "rgb(180, 194, 75)",
            "font-family" => "Tahoma",
            "font-size" => "26px",
            "margin-bottom" => "26px"
        },
        title
    ]
}
