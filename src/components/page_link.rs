
use seed;
use seed::prelude::*;
use crate::Msg;


pub fn view(link_name: &str, href: &str) -> Node<Msg> {
    seed::a![
        link_name,
        seed::attrs!{
            At::Href => href,
            At::Class => "on-link-hover"
        },
        seed::style!{
            "display" => "block",
            "color" => "rgb(85, 119, 153)",
            "font-family" => "Georgia",
            "margin-top" => "8px",
            "margin-bottom" => "8px",
            "cursor" => "pointer"
        }
    ]
}
