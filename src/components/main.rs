use crate::components;
use crate::Model;
use crate::Msg;
use crate::PageEnum;
use seed;
use seed::prelude::*;

pub fn view(model: &Model) -> Node<Msg> {
    match model.is_small_screen() {
        true => view_small_screen(model),
        false => view_normal_screen(model),
    }
}

fn view_small_screen(model: &Model) -> Node<Msg> {
    return seed::div![
        seed::style! {
            "display" => "flex",
            "flex-direction" => "column",
            "padding" => "10px 10px 10px 10px"
        },
        view_page_links(),
        seed::br![],
        view_selected_page(model)
    ];
}

fn view_normal_screen(model: &Model) -> Node<Msg> {
    return seed::div![
        seed::style! {
            "display" => "flex",
            "flex-direction" => "column"
        },
        view_normal_screen_header(),
        view_normal_screen_middle(model),
        view_normal_screen_footer()
    ];
}

fn view_normal_screen_header() -> Node<Msg> {
    return seed::div![seed::style! {
        "flex-basis" => "100px"
    }];
}

fn view_normal_screen_middle(model: &Model) -> Node<Msg> {
    return seed::div![
        seed::style! {
            "display" => "flex",
            "flex-direction" => "row"
        },
        view_normal_screen_middle_left(model),
        view_normal_screen_middle_right(),
    ];
}

fn view_normal_screen_middle_left(model: &Model) -> Node<Msg> {
    return seed::div![
        seed::style! {
            "flex-basis" => "600px",
            "flex-grow" => "2",
            "flex-shrink" => "1",
            "display" => "flex",
            "flex-direction" => "row"
        },
        seed::div![seed::style! {
            "flex-basis" => "100px",
            "flex-grow" => "1",
            "min-width" => "10px",
        },],
        seed::div![
            seed::style! {
                "flex-basis" => "400px",
                "flex-grow" => "2",
            },
            view_selected_page(model)
        ],
        seed::div![seed::style! {
            "flex-basis" => "100px",
            "flex-grow" => "1",
            "min-width" => "30px",
        },],
    ];
}

fn view_normal_screen_middle_right() -> Node<Msg> {
    return seed::div![
        seed::style! {
            "flex-basis" => "200px",
            "flex-grow" => "1",
            "flex-shrink" => "0",
            "margin-top" => "50px",

        },
        view_page_links()
    ];
}

fn view_normal_screen_footer() -> Node<Msg> {
    return seed::div![seed::style! {
        "flex-basis" => "100px"
    }];
}

fn view_selected_page(model: &Model) -> Node<Msg> {
    return match model.page {
        PageEnum::Welcome => components::pages::welcome::view(),
        PageEnum::AboutMe => components::pages::about_me::view(),
        PageEnum::InspirationalPeople => components::pages::inspirational_people::view(),
        // PageEnum::Thoughts(thoughts_page) => components::pages::thoughts::view(thoughts_page),
    };
}

fn view_page_links() -> Vec<Node<Msg>> {
    return vec![
        components::pages::welcome::view_page_link(),
        components::pages::about_me::view_page_link(),
        // components::pages::thoughts::view_page_link(),
        components::pages::inspirational_people::view_page_link(),
    ];
}
