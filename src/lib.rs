use seed;
use seed::prelude::*;
use web_sys;

mod components;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum ThoughtsPageEnum {
    Main,
    Existance,
    Morality,
}

impl ThoughtsPageEnum {
    pub fn href(&self) -> String {
        match self {
            ThoughtsPageEnum::Existance => "existance".to_owned(),
            ThoughtsPageEnum::Morality => "morality".to_owned(),
            _ => "".to_owned(),
        }
    }

    pub fn from_url_hash(url_hash: Option<&str>) -> Self {
        seed::log!("ThoughtsPageEnum::from_url_hash", url_hash);
        match url_hash {
            Some("existance") => ThoughtsPageEnum::Existance,
            Some("morality") => ThoughtsPageEnum::Morality,
            _ => ThoughtsPageEnum::Main,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum PageEnum {
    Welcome,
    AboutMe,
    // Thoughts(ThoughtsPageEnum),
    InspirationalPeople,
}

fn split_url_hash(url_hash: Option<&str>) -> (Option<&str>, Option<&str>) {
    if url_hash.is_none() {
        return (None, None);
    }

    let mut hash_left = url_hash.unwrap();
    let mut hash_right = Option::None;
    let delimiter_i = hash_left.find("/");
    if let Some(delimiter_i) = delimiter_i {
        let parts = hash_left.split_at(delimiter_i);
        hash_left = parts.0;
        hash_right = Some(parts.1.split_at(1).1);
    }

    return (Some(hash_left), hash_right);
}

impl PageEnum {
    pub fn href(&self) -> String {
        match self {
            PageEnum::Welcome => "/#welcome".to_owned(),
            PageEnum::AboutMe => "/#about_me".to_owned(),
            PageEnum::InspirationalPeople => "/#inspirational_people".to_owned(),
            // PageEnum::Thoughts(thoughts_page) => {
            //     format!("{}{}", "/#thoughts/", thoughts_page.href())
            // }
        }
    }

    pub fn from_url_hash(url_hash: Option<&String>) -> Self {
        seed::log!("PageEnum::from_url_hash", url_hash);
        let (hash_left, _hash_right) = split_url_hash(url_hash.map(String::as_ref));

        match hash_left {
            // Some("thoughts") => PageEnum::Thoughts(ThoughtsPageEnum::from_url_hash(hash_right)),
            Some("inspirational_people") => PageEnum::InspirationalPeople,
            Some("about_me") => PageEnum::AboutMe,
            _ => PageEnum::Welcome,
        }
    }
}

pub enum Msg {
    UrlChanged(subs::UrlChanged),
    WindowResize(f64),
    None,
}

pub struct Model {
    pub page: PageEnum,
    pub inner_width: f64,
    pub window_resize_stream: StreamHandle,
}

impl Model {
    pub fn is_small_screen(&self) -> bool {
        return self.inner_width > 0_f64 && self.inner_width < 600_f64;
    }
}

fn init(url: Url, orders: &mut impl Orders<Msg>) -> Model {
    orders.subscribe(Msg::UrlChanged);
    let window_resize_stream =
        orders.stream_with_handle(streams::window_event(
            Ev::Resize,
            |_| match get_inner_width() {
                Some(w) => Msg::WindowResize(w),
                None => Msg::None,
            },
        ));

    let w = match get_inner_width() {
        Some(w) => w,
        None => -1_f64,
    };

    return Model {
        page: PageEnum::from_url_hash(url.hash()),
        inner_width: w,
        window_resize_stream: window_resize_stream,
    };
}

fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>) {
    match msg {
        Msg::UrlChanged(subs::UrlChanged(url)) => {
            model.page = PageEnum::from_url_hash(url.hash());
        }
        Msg::WindowResize(inner_width) => model.inner_width = inner_width,
        Msg::None => {
            orders.skip();
        }
    }
}

fn get_inner_width() -> Option<f64> {
    let w = web_sys::window();
    if w.is_none() {
        seed::error!("w.is_none() 1");
        return None;
    }
    let w = w.unwrap();
    let w = w.inner_width();
    if w.is_err() {
        seed::error!("w.is_err() 2");
        return None;
    }
    let w = w.unwrap();
    return w.as_f64();
}

#[wasm_bindgen(start)]
pub fn start() {
    App::start("app", init, update, components::main::view);
}
